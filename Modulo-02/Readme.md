![Git como sistema de restaurate](Media/git-como-un-sistema-restaurante.jpg)

## Módulo-02: Un primer ejemplo: `hola git`

### Contenido

1. Creando tu primer repositorio (repo)
2. Conociendo los estados de un repo
3. Agregando archivos a un repo
4. Excluyendo archivos de un repo
5. Conociendo las etapas de un archivo en un repo

### Desarrollo

1. Introducción al proyecto `hola git`
2. Creando nuestra carpeta de proyecto `hola-git`
3. Creando el archivo `README.md` del proyecto
4. Creando el archivo `hola/README.md`
5. Inicializando el repositorio (repo)
6. Estados de un repo
7. Adicionando todos los archivos al área de montaje
8. Adicionando el área de montaje al repo
9. Revisando de nuevo el estado del repo
10. Creando el archivo `amigos/README.md`
11. Creando el archivo `amigos/amiguis.md`
12. Excluye el archivo `amigos/amiguis.md` del repo
13. Adicionando todos los archivos al área de montaje
14. Creando una confirmación al repo
15. Verificando el estado del repo
16. Crea el archivo `amigos/lista-negra.txt`
16. Crea el archivo `enemigos/README.md`
17. Crea el archivo `enemigos/que-les-caiga-un-rayo.csv`
18. Excluye todos los archivo de la carpeta `enemigos/` del repo
19. Adiciona todos los archivos al área de preparación
20. Guarda los cambios en el repo
21. Verifica que e repo tenga un estado limpio

