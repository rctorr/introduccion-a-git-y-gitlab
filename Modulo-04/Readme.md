![Git y gitlab](Media/git-gitlab.png)

## Módulo-04: Conectando repo local con repo en GitLab

### Contenido

1. Repaso

### Desarrollo

1. Repaso

   1. Módulo 02: `git init`, `git add .`, `git commit -m "mensaje"`, `git status`, `git status -u`, `.gitignore`
   2. Módulo 03: `git log`, `git log -p`, `git log --stat --pretty=oneline --graph`, `git checkout -b rama`, `git add archivo/carpeta`, `git checkout rama`, `git merge --no--ff preparando-cafe`, `git branch -a`, `git branch -d preparando-cafe`, `git checkout -- desayuno/README.md`

2. Requisitos

   1. Contar con cuenta en [GitLab](https://gitlab.com)
   2. Contar con un repo local (ej. `hola-git/`)
   3. Git instalado en el equipo local [Descarga de Git](https://www.git-scm.com/downloads)

1. Creando un repo en GitLab

   1. Conectarse a GitLab con su cuenta [GitLab Login](https://gitlab.com/users/sign_in)
   2. Dar click en el símbolo de + para crear un nuevo repo [Creando nuevo repo](https://gitlab.com/projects/new)
   3. Dar click en crear proyecto en blanco [Creando proyecto en blanco](https://gitlab.com/projects/new#blank_project)
   4. Asignar nombre al repo (ej. `hola-git`)
   5. Definir si el repo será público o privado
   6. Y como vamos a usar un repo local ya creado dehabilitar la opción de crear el archivo README
   7. Dar click en **Create Project**

2. Creando llave pública

3. Cargando llave pública a GitLab

4. Vinculando el repo local con remoto en GitLab

5. Sincronizando cambios de la rama `master` del repo local al repo remoto

    1. `git push -u origin master`

6. Revisando ramas locales y remotas

   1. `git branch -a`
  


