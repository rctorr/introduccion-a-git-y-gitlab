![Git y Github](Media/git-y-gitlab.jpg)

## Trabajando con Git y GitLab

## Instructor:

- Ricardo Torres
- Email: rictor@cuhrt.com
- GitLab: https://gitlab.com/rctorr
- Twitter: https://twitter.com/rctorr
- Lenguajes: C, HTML, CSS, PHP, SQL, Javascript, Python

### Temario
 
 - [Módulo 01: Introducción al curso](Modulo-01/Readme.md)
 - [Módulo-02: Un primer ejemplo: `hola git`](Modulo-02/Readme.md)
 - [Módulo-03: Trabajando con ramas (branch) en git](Modulo-03/Readme.md)
 - [Módulo-04: Conectando repo local con repo en GitLab](Modulo-04/Readme.md)
 - [Módulo-05: Trabajando con ramas locales y remotas](Modulo-04/Readme.md)

### Repositorio

- [Repositorio](https://gitlab.com/rctorr/introduccion-a-git-y-gitlab)

