![Git](Media/git.jpg)

## Módulo 01: Introducción al curso

### Contenido

1. Descripción general
2. Antes de empezar
3. ¿Qué es el sistema de control de versiones?
4. Validando la instalación de Git en Linux
5. Validando la instalación de Git en Mac OS
6. Validando la instalación de Git en Windows
7. Trabajando con Git o Git Bash

### 1. Descripción general

Vamos a aprender a usar un sistema de control de versiones creando repositorios locales y remotos, tanto individual como para trabajo en equipo.

### 2. Antes de empezar

Para realizar éste curso se requiere contar con:

1. Conocimientos del sistema operativo a usar Linux, Mac o Windows
2. Conocimientos de los comandos básicos para manejar archivos y carpetas desde la **Terminal**
3. Git ya instalado en tu sistema operativo.
3. Cuenta en Gitlab (https://gitlab.com/-/trial_registrations/new)
4. Editor de código [Vim](https://www.vim.org/), [Emacs](https://www.gnu.org/software/emacs/), [Sublime Text](https://www.sublimetext.com/), [PyCharm o JetBrains](https://www.jetbrains.com/pycharm/), [Notepad++](https://notepad-plus-plus.org/), [Jupyter Lab](https://jupyter.org/), [VS Code](https://code.visualstudio.com/)

### 3. ¿Qué es el sistema de control de versiones?

Para entender lo que hace un sistema de control de versiones primero veamos el porqué se necesita partiendo del problema:

> ¿Necesitas escribir `<código>` con un **grupo de personas**?

Posiblemente te haya sucedido algua de las siguiente situaciones:

- Has usado archivos de la forma `la-foto-del-recuerdo-prueba-prueba2-sin-fondo-con-corazones-...jpg` y dices por dios, ya no le pongan ni una raya más al tigre!
- O si alguien modificó un archivo sin previo aviso o posiblemente hasta borró un archivo importante y no se sabe quien y cuando sucedió
- Finalmente a nivel personal has hecho varias modificaciones a varios archivos de un proyecto y ha quedado sin funcionar sin saber porque y necesitas regresar a un estado en el que **aún funcionaba**

La solución a estas y otros problemas incluyendo un plus es usar un sistema de control de versiones que permite:

1. Llevar un registro de tus cambios realizados a una carpeta de archivo dwe algún proyecto.
2. Permite realizar desarrollo y/o edición colaborativa de archivos.
3. Permite conocer quien y cuando realizó un cambio
4. Permite deshacer cualquier cambio y regresar a cualquier punto de la historia
5. Permite organizar el flujo de trabajo del proyecto por medio del uso de **ramas** (branch)
6. Permite tener una visión en todo momento del estado de desarrollo de un proyecto.

Existen 3 tipos de control de versiones:

1. Local
![Local VCS](Media/local-vcs.png)
2. Centralizado
![Centralizado VCS](Media/centralizado-vcs.png)
3. Distribuido
![Distribuido VCS](Media/distribuido-vcs.png)

Existen varios sistemas de control de versiones:

1. **Git** 
2. Subversion
3. Mercurial
4. Perforce

### 4. Validando la instalación de Git en Linux

Por si aún no has instalado **Git** en distribuciones de Linux derivadas de Debian o Ubuntu puedes usar el siguiente comando en una **Terminal**:
```shell
$ sudo apt install git
...
```
Luego para validar la instalación ejecutar el siguiente comando:
```shell
$ git --version
git version 2.17.1
$ 
```
Listo, eso indica que **Git** está instalado y listo para usar.

### 5. Validando la instalación de Git en Mac OS

Por si aún no has instalado **Git** en tu Mac lo puedes hacer usando el siguiente comando en una **Terminal**:
```shell
$ brew install git
...
```
Luego para validar la instalación ejecutar el siguiente comando en una **Terminal**:
```shell
$ git --version
git version 2.17.1
$ 
```
Listo, eso indica que **Git** está instalado y listo para usar.

### 6. Validando la instalación de Git en Windows

Por si aún no lo has instalado para instalar **Git** wn Windows hay que descargarlo desde la dirección: https://git-scm.com/download/win

Ejecuta el instalador, se pueden usar las opciones por **default**

Tras instalar se puede iniciar la aplicación **git bash** lo que abrirá una ventana que nos permitirá ejecutar git y para corroborar ejecuta el comando:
```shell
$ git --version
git version 2.17.1
$ 
```

![git bash](Media/git-bash.png)


### 7. Trabajando con Git o Git Bash

En nuestro equipo local iniciamos una **Terminal** o **Git bash** y realizamos la configuración inicial de git con los siguientes comandos:

```shell
git config --global user.name "tu nombre de usuario"
git config --global user.email "tu dirección de email"
```

Por ejemplo, en mi caso:

```shell
git config --global user.name "rctorr"
git config --global user.email "rictor@cuhrt.com"
```

Para mostrar la configuración se puede ejecutar:

```
git config --global user.name
git config --global user.email
```

Si deseas ver todas las configuraciones de git se usa el comando:

```
git config --list
```
