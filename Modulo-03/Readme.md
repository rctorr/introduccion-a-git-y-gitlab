![Git como sistema de restaurate](Media/git-ramas.png)

## Módulo-03: Trabajando con ramas (branch) en git 

### Contenido

1. Creando tu primer repositorio (repo)
2. Conociendo los estados de un repo
3. Agregando archivos a un repo
4. Excluyendo archivos de un repo
5. Conociendo las etapas de un archivo en un repo

### Desarrollo

1. Repaso

   2. Creando repo: `git init`
   3. Agregando todos los archivos al área de montaje: `git add .`
   4. Confirmando cambios / agregando cambios al repo: `git commit -m "mensaje"`
   5. Conocer el estado del repo: `git status`
   6. Conocer el estado del repo incluyendo archivos de carpetas sin seguimiento: `git status -u`
   7. Archivo para colocar listas de archivos o carpetas ignorados para el repo: `.gitignore`

2. Conociendo el estado de la carpeta/repo de trabajo

   2. `git status`
   3. `git log`
   4. `git log -p`
   5. `git log --stat`
   6. `git log --pretty=oneline`
   7. `git log --pretty=oneline --stat`
   8. `git log --graph`

2. Creando una rama para la tarea **preparando café**:

   3. `git checkout -b preparando-cafe`
   4. Crea la carpeta `desayuno/`
   3. Creando el archivo `README.md` de la carpeta creada
   4. Creando el archivo `desayuno/preparando-cafe.md`
   5. Agrega la lista de pasos para preparar un buen café al archivo `preparando-cafe.md`
   6. Revisando el estado: `git status`
   7. Agregando los archivo al montaje indicando la carpeta:
      8. `git add desayuno/`
   9. Guardando el montaje en el repo: `git commit -m "..."`
   
10. Revisando la bitácora de git: `git log --stat --graph`
11. Haciendo que la rama de trabajo actual sea `master`

    12. `git checkout master`
    
13. Fusionando los cambios de la rama `preparando-cafe` a la rama `master`

    14. `git merge --no--ff preparando-cafe`
    15. Guardando el mensaje de la fusión
    16. Revisando de nuevo con: `git log --graph`
    
17. Listado de ramas

    18. `git branch -a`
    19. Identificando rama actual
    20. Identificando ramas locales
    21. Identificando ramas remotas

17. Eliminando la rama `preparando-cafe`

    18. `git branch -d preparando-cafe`
    19. Verificando imprimiendo la lista de ramas, sólo debería existir `master`

20. Recuperando el estado de un archivo o carpeta

    21. Elimina el archivo `desayuno/README.md`
    22. Revisa el estado del repo: `git status`
    23. Recupera el archivo con: `git checkout -- desayuno/README.md`
    24. Revisa el estado y la lista de archivos
    25. Elimina la carpeta completa `desayuno/` y revisa que no exista en la lista de archivos, entonces recupera la carpeta con todos sus archivos.

26. Creando un repo en gitLab
27. Vinculando el repo remoto de GitLab con nuestro repo local
28. Sincronizando cambios de la rama `master` del repo local al repo remoto
    29. `git push -u origin master`
