![Git como sistema de restaurate](Media/git-ramas.png)

## Módulo-04: Trabajando con ramas locales y remotas

### Contenido

1. Repaso
2. Repaso infografía

### 1. Repaso

   1. Módulo 02: `git init`, `git add .`, `git commit -m "mensaje"`, `git status`, `git status -u`, `.gitignore`
   2. Módulo 03: `git log`, `git log -p`, `git log --stat --pretty=oneline --graph`, `git checkout -b rama`, `git add archivo/carpeta`, `git checkout rama`, `git merge --no--ff preparando-cafe`, `git branch -a`, `git branch -d preparando-cafe`, `git checkout -- desayuno/README.md`
   3. Módulo 04: **Creando repo en GitLab**, **Creando llave público-privada**, **Agregando llave pública a GitLab**, `git remote add ...`, `git push -u origin rama`, `git branch -a`, **Revisar repo en GitLab**

### 2. Repaso infografía

   1. Proceso de git en una carpeta local ![Proceso git carpeta local](Media/proceso-git-carpeta-local.png)
   2. Registro histórico en repo local ![Registro historico repo local](Media/registro-historico-repo-local.png)
   3. Registro histórico en repo local y remoto ![Registro historico repo local y remoto](Media/registro-historico-repo-local-y-remoto.png)

### 3. Iniciando jonada de trabajo

1. Revisando estado del repo, rama local actual, rama remota vinculada

   ```shell
   git status
   ...
   ```

2. Revisando ramas locales y remotas

   ```shell
   git branch -a
   ...
   ```

3. Revisando bitácora de git

   ```shell
   git log --pretty=oneline
   ...
   ```

### 4. Creando una rama local para una tarea, agregando cambios y actualizando la rama

1. Creando una rama para la tarea **preparando-molletes** y nos cambiamos a esa rama:

   ```shell
   git checkout -b preparando-molletes
   ```
   
2. Usa la carpeta `desayuno/`

3. Crea el archivo `desayuno/preparando-molletes.md` y agrega la lista de pasos para preparar unos sabrosos molletes de frijoles y queso.

4. Revisa el estado: `git status`

5. Agrega el archivo al área de montaje usando el nombre de la carpeta:

   ```shell
   git add desayuno
   ...
   ```

6. Guarda el área de montaje en el repo: `git commit -m "..."`

### 4. Revisando diferencias entre rama local y remota y actualizando rama remota
   
1. Revisando la bitácora de git y compara la diferencias de avance entre la rama local y la remota

   ```shell
   git log --stat --graph
   ...
   ```
   
2. Publica los cambios de la ramma **preparando-molletes** en el repo

   ```shell
   git push -u origin preparando-molletes
   ...
   ```

3. Visualiza la rama **preparando-molletes** en Gitlab y comparte el link de la rama

### 5. Fusionando la rama **preparando-molletes** en la rama destino **master**

1. Fusionando los cambios de la rama `preparando-molletes` a la rama `master`

   ```shell
   git checkout master
   ...
   git merge --no--ff preparando-molletes
   ...
   ```
   La instrucción anterior podría mostrar un mensaje en el editor configurado por omisión, por lo que hay que revisar el mensaje, guardarlo y salir del editor si no hay necesidad de cambiar nada.

2. Revisando de nuevo el estado de la rama **master** con: `git log --graph`

### 6. Limpiando o eliminando ramas

1. Eliminando la rama `preparando-molletes` en local que ya no es necesaria porque ya ha sido fusionada a la rama **master**

   ```shell
   git branch -d preparando-molletes
   ...
   ```

2. Verificando la lista de ramas, sólo debería existir `master`

3. Eliminando la rama `preparando-molletes` en GitLab (remoto)

   ```shell
   git push origin --delete preparando-molletes
   ...
   ```
   
4. Verificando la lista de ramas, sólo debería existir `master` tanto en local como en remoto

### 7. Recuperando o trasladando la carpeta de un proyecto

1. Elimina la carpeta `hola-git/` completa

2. Clona el repo desde GitLab con: `git clone <dirección del repo>`

3. Verifica que toda la carpeta de trabajo y archivos esté disponible

4. Vive feliz!

